<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\Roles;
use App\Models\RoleUser;
use App\Models\RoleUsers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class UsersController extends AdminController
{
    public function index(){
        if(!Auth::check()){
            $redirect_url = Session::get('redirect_url');
            return view('Admin.Auth.login',[
                'page_title' =>'Login',
                'redirect_url' => $redirect_url,
            ]);
        }else{
            return redirect('admin/dashboard');
        }

    }

    public function postLogin(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];

        //Validation rules
        $v = Validator::make($request->all(), $rules);

        //If validation fails
        if ($v->fails()) {
            return Redirect::back()->withErrors($v)->withInput();
        };

        $rememberMe = $request->has('remember');

        //Check User Authorisation
        $authorised = Auth::attempt([
            'email' => $request->email,
            'password' => $request->password,
            'is_admin' => 1,
        ], false);

        if ($authorised) {
            if (!Auth::user()->active) {
                //Logout
                Auth::logout();
                return redirect('admin/login')
                    ->with(['msg_type' => 'danger', 'msg' => 'Your user account is marked as inactive.']);
            } else {
                return redirect($request->redirect_url)->with(['msg_type'=>'success','msg'=>'Logged in Successfully']);
            }
        } else {
            return redirect('/admin/login')
//            ->with('error', '');
                ->with(['msg_type' => 'danger', 'msg' => 'There was a problem logging you in, please try again.']);
        }

    }

    public function logout()
    {
        if (Auth::check()) {
            Auth::logout();
        }

        return redirect('admin/login')->with(['msg_type' => 'success', 'msg' => 'You have successfully been logged out.']);
    }

    public function userIndex(Request $request){
        if(checkPermission('admin.users.list')){
            $this->setDatatableJSCSS();
            $breadcrumbData = [
                [
                    'title'=>'Dashboard',
                    'url' => route('dashboard'),
                    'active' => false,
                ],
                [
                    'title' => 'Admin Users',
                    'url'=> '#',
                    'active'=> true,

                ]
            ];

            return view('Admin.Auth.Users.index',[
                'page_title' =>'Admin Users',
                'menu' => 'authentication',
                'submenu' => 'users',
                'breadcrumbData'=> $breadcrumbData,
            ]);

        }else{
            return Redirect::back()->with(['msg_type'=>'error','msg'=>'you are not allowed to access']);
        }
    }
    public function userList(Request $request){

        return DataTables::eloquent(User::query()->whereNotIn('id',[Auth::user()->id])->where('is_admin',1))

            ->editColumn('name',function ($query){
                return '<strong>'.$query->name.'</strong>';
            })
            ->addColumn('action', function ($query) {
                $links = '';
                if(checkPermission('admin.users.update')) {
                    $links .= '<a data-toggle="tooltip" title="Edit" href="' . route('adminUsers.edit', $query->id) . '" class="btn btn-sm btn-primary-custom"> <i class="fa fa-edit"></i></a>';
                }
                if(checkPermission('admin.users.view')) {
                    $links .= '&nbsp;<a data-toggle="tooltip" title="View" href="' . route('adminUsers.view', $query->id) . '" class="btn btn-sm btn-primary"> <i class="fa fa-eye"></i></a>';
                }
                if(checkPermission('admin.users.delete')) {
                    $links .= '&nbsp;<button class="btn btn-sm btn-danger" onclick="deleteUser($(this),'.$query->id.')"><i class="fa fa-trash"></i> </button>';
                }
                return $links;
            })
            ->escapeColumns([])
            ->toJson();
    }
    public function create(){
        if(checkPermission('admin.users.create')){
            $this->loadSelect2();
            $breadcrumbData = [
                [
                    'title'=>'Dashboard',
                    'url' => route('dashboard'),
                    'active' => false,
                ],
                [
                    'title' => 'Admin Users',
                    'url'=> route('adminUsers'),
                    'active'=> false,

                ],
                [
                    'title' => 'Create',
                    'url'=> '#',
                    'active'=> true,
                ]
            ];

            return view('Admin.Auth.Users.create',[
                'page_title' => 'Create User',
                'menu' => 'authentication',
                'submenu' => 'users',
                'breadcrumbData' => $breadcrumbData,
                'roles' => Roles::all()->pluck('name','id'),
            ]);
        }else{
            return Redirect::back()->with(['msg_type'=>'error','msg'=>'you are not allowed to access']);
        }

    }

    public function edit($id){
        if(checkPermission('admin.users.update')){
            if($user = User::find($id)){
                $this->loadSelect2();
                $breadcrumbData = [
                    [
                        'title'=>'Dashboard',
                        'url' => route('dashboard'),
                        'active' => false,
                    ],
                    [
                        'title' => 'Admin Users',
                        'url'=> route('adminUsers'),
                        'active'=> false,
                    ],
                    [
                        'title' => 'Edit',
                        'url'=> '#',
                        'active'=> true,
                    ]
                ];
                return view('Admin.Auth.Users.create',[
                    'page_title' => 'Update User',
                    'menu' => 'authentication',
                    'submenu' => 'users',
                    'breadcrumbData' => $breadcrumbData,
                    'user' => $user,
                    'roles' => Roles::all()->pluck('name','id'),
                ]);
            }else{
                return Redirect::back()->with(['msg_type' => 'success','msg' => 'Item was not found']);
            }
        }else{
            return Redirect::back()->with(['msg_type'=>'error','msg'=>'you are not allowed to access']);
        }
    }
    public function view($id){
        if(checkPermission('admin.users.view')){
            $breadcrumbData = [
                [
                    'title'=>'Dashboard',
                    'url' => route('dashboard'),
                    'active' => false,
                ],
                [
                    'title' => 'Admin Users',
                    'url'=> route('adminUsers'),
                    'active'=> false,
                ],
                [
                    'title' => 'View',
                    'url'=>'',
                    'active'=> true,
                ]
            ];
            $this->setDatatableJSCSS();
            $user = User::find($id);
            //        dd($user->saleCardListingOrders);
            if($user){
                return view('Admin.Auth.Users.view',[
                    'page_title' =>'User Detail',
                    'menu' =>'authentication',
                    'submenu' =>'users',
                    'user'=> $user,
                    'breadcrumbData'=> $breadcrumbData,
                ]);
            }
            return Redirect::back()->with(['msg_type'=>'danger','msg'=>'User Not Found']);
        }else{
            return Redirect::back()->with(['msg_type'=>'error','msg'=>'you are not allowed to access']);
        }

    }

    public function save(Request  $request,$id = null){
        if($id){
            $user = User::find($id);
            if($request->name){
                $user->update(['name'=>$request->name]);
            }
            if($request->username){
                $user->update(['username'=>$request->username]);
            }
            if($request->email){
                $user->update(['email'=>$request->email]);
            }
            if($request->password){
                $user->update(['password'=> Hash::make($request->password)]);
            }
            if($request->bio){
                $user->update(['password'=> Hash::make($request->bio)]);
            }
            if(empty($request->old_image_url)){
            $user->unlinkImage();
              }
//            if($request->hasFile('image')){
//                $user->uploadImage();
//            }

            // manage roles by user id
//            if($request->roles){
                RoleUsers::where('user_id',$id)->delete();
//                foreach ($request->roles as $role_id){
                    RoleUsers::create(['role_id' =>1,'user_id'=>$id]);
//                }
//            }
            return redirect('admin/admin-users')->with(['msg_type' => 'success','msg' => 'User Updated']);

        }else{
            $validator = Validator::make($request->all(),[
                'username' =>'unique:users,username',
                'email' =>'unique:users,email',
            ]);
            if($validator->fails()){
                return Redirect::back()->withErrors($validator)->withInput();
            }else{
                $request->request->add([
                    'is_admin' => 1,
                    'active' => 1,
                    'password' => Hash::make($request->password),
                ]);

                $user =  User::make($request->all());
//                if($request->hasFile('image')){
//                    $user->uploadImage();
//                }
                $user->save();
                // manage roles by user id
                RoleUsers::where('user_id',$user->id)->delete();
//                foreach ($request->roles as $role_id){
                    $role = RoleUsers::create(['role_id' => 1,'user_id' => $user->id]);
//                }
                return redirect('admin/admin-users')->with(['msg_type' => 'success','msg' => 'User Created']);
            }
        }

    }
    public function delete($id){
        $response = [];
        if($user = User::find($id)){
            $user->delete();
            $response = [
                'status' => 1,
                'message' => 'Item deleted',
            ];
        }else{
            $response = [
                'status' => 0,
                'message' => 'Item not found'
            ];
        }
        return $response;
    }
}
