<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use App\Models\Roles;
use App\Models\RoleUsers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class EmployeeController extends AdminController
{
    public function index(){
        if(checkPermission('admin.employee.users.list')){
            $this->setDatatableJSCSS();
            $breadcrumbData = [
                [
                    'title'=>'Dashboard',
                    'url' => route('dashboard'),
                    'active' => false,
                ],
                [
                    'title' => 'Employees',
                    'url'=> '#',
                    'active'=> true,
                ]
            ];

            return view('Admin.Employee.index',[
                'page_title' =>'Employees',
                'menu' => 'employee',
                'breadcrumbData'=> $breadcrumbData,
            ]);

        }else{
            return Redirect::back()->with(['msg_type'=>'error','msg'=>'you are not allowed to access']);
        }
    }
    public function employeesList(Request $request){

        return DataTables::eloquent(User::query()->whereNotIn('id',[Auth::user()->id])->where('is_employee',1))
            ->editColumn('name',function ($query){
                return '<strong>'.$query->name.'</strong>';
//                return ($query->check_file_exists ? '<img src="'.$query->thumbnail_url.'" style="width:100%;"/>':'').'<strong>'.$query->name.'</strong>';
            })
            ->addColumn('action', function ($query) {
                $links = '';
                if(checkPermission('admin.employee.users.update')) {
                    $links .= '<a data-toggle="tooltip" title="Edit" href="' . route('employee.edit', $query->id) . '" class="btn btn-sm btn-primary-custom"> <i class="fa fa-edit"></i></a>';
                }
                if(checkPermission('admin.employee.users.view')) {
                    $links .= '&nbsp;<a data-toggle="tooltip" title="View" href="' . route('employee.view', $query->id) . '" class="btn btn-sm btn-primary"> <i class="fa fa-eye"></i></a>';
                }
                if(checkPermission('admin.employee.users.delete')) {
                    $links .= '&nbsp;<button class="btn btn-sm btn-danger" onclick="deleteUser($(this),'.$query->id.')"><i class="fa fa-trash"></i> </button>';
                }
                return $links;
            })
            ->escapeColumns([])
            ->toJson();
    }
    public function create(){
        if(checkPermission('admin.employee.users.create')){
            $this->loadSelect2();
            $breadcrumbData = [
                [
                    'title'=>'Dashboard',
                    'url' => route('dashboard'),
                    'active' => false,
                ],
                [
                    'title' => 'Employees',
                    'url'=> route('employee.list'),
                    'active'=> false,

                ],
                [
                    'title' => 'Create',
                    'url'=> '#',
                    'active'=> true,
                ]
            ];

            return view('Admin.Employee.create',[
                'page_title' => 'Create Employee',
                'menu' => 'employee',
                'breadcrumbData' => $breadcrumbData,
            ]);
        }else{
            return Redirect::back()->with(['msg_type'=>'error','msg'=>'you are not allowed to access']);
        }

    }

    public function edit($id){
        if(checkPermission('admin.employee.users.update')){
            if($employee = User::find($id)){
                $this->loadSelect2();
                $breadcrumbData = [
                    [
                        'title'=>'Dashboard',
                        'url' => route('dashboard'),
                        'active' => false,
                    ],
                    [
                        'title' => 'Employees',
                        'url'=> route('employee.list'),
                        'active'=> false,
                    ],
                    [
                        'title' => 'Update',
                        'url'=> '#',
                        'active'=> true,
                    ]
                ];
                return view('Admin.Employee.create',[
                    'page_title' => 'Update Employee',
                    'menu' => 'employee',
                    'breadcrumbData' => $breadcrumbData,
                    'employee' => $employee,
                ]);
            }else{
                return Redirect::back()->with(['msg_type' => 'success','msg' => 'Employee was not found']);
            }
        }else{
            return Redirect::back()->with(['msg_type'=>'error','msg'=>'you are not allowed to access']);
        }
    }
    public function view($id){
        if(checkPermission('admin.employee.users.view')){
            $breadcrumbData = [
                [
                    'title'=>'Dashboard',
                    'url' => route('dashboard'),
                    'active' => false,
                ],
                [
                    'title' => 'Employees',
                    'url'=> route('employee.list'),
                    'active'=> false,
                ],
                [
                    'title' => 'View',
                    'url'=>'',
                    'active'=> true,
                ]
            ];
            $this->setDatatableJSCSS();
            $employee = User::find($id);
            //        dd($user->saleCardListingOrders);
            if($employee){
                return view('Admin.Employee.view',[
                    'page_title' =>'Employee Detail',
                    'menu' =>'employee',
                    'employee'=> $employee,
                    'breadcrumbData'=> $breadcrumbData,
                ]);
            }
            return Redirect::back()->with(['msg_type'=>'danger','msg'=>'Employee Not Found']);
        }else{
            return Redirect::back()->with(['msg_type'=>'error','msg'=>'you are not allowed to access']);
        }

    }

    public function save(Request  $request,$id = null){
        if($id){
            $employee = User::find($id);
            if($request->name){
                $employee->update(['name'=>$request->name]);
            }
            if($request->username){
                $employee->update(['username'=>$request->username]);
            }
            if($request->email){
                $employee->update(['email'=>$request->email]);
            }
            if($request->password){
                $employee->update(['password'=> sha1($request->password)]);
            }
            if($request->bio){
                $employee->update(['bio'=> $request->bio]);
            }
//            if(empty($request->old_image_url)){
//                $employee->unlinkImage();
//            }


            // manage roles by user id
//            if($request->roles){
//                RoleUsers::where('user_id',$id)->delete();
//                foreach ($request->roles as $role_id){
//                    RoleUsers::create(['role_id' => $role_id,'user_id'=>$id]);
//                }
//            }
            return redirect('admin/employee')->with(['msg_type' => 'success','msg' => 'Employee Updated']);

        }else{
            $validator = Validator::make($request->all(),[
                'username' =>'unique:users,username',
                'email' =>'unique:users,email',
            ]);
            if($validator->fails()){
                return Redirect::back()->withErrors($validator)->withInput();
            }else{
                $request->request->add([
                    'is_employee' => 1,
                    'is_admin' => 1,
                    'active' => 1,
                    'password' => sha1($request->password),
                ]);

                $employee =  User::make($request->all());
                $employee->save();
//                if($request->hasFile('image')){
//                    $employee->uploadImage();
//                }

                // manage roles by user id
                RoleUsers::where('user_id',$employee->id)->delete();
//                foreach ($request->roles as $role_id){
                $role = RoleUsers::create(['role_id' => 5,'user_id' => $employee->id]);
//                }
                return redirect('admin/employee')->with(['msg_type' => 'success','msg' => 'Employee Created']);
            }
        }

    }
    public function delete($id){
        $response = [];
        if($user = User::find($id)){
            $user->delete();
            $response = [
                'status' => 1,
                'message' => 'Employee deleted',
            ];
        }else{
            $response = [
                'status' => 0,
                'message' => 'Employee not found'
            ];
        }
        return $response;
    }
}
