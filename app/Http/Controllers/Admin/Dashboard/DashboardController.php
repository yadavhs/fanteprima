<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends AdminController
{
    public function index(){
        $breadcrumbData = [
            [
                'title'=>'Dashboard',
                'url' => route('dashboard'),
                'active' => true,
            ],
        ];
        return view('Admin.dashboard.index',[
            'page_title' => 'Dashboard',
            'menu' => 'dashboard',
            'breadcrumbData' => $breadcrumbData,
        ]);
    }
}
