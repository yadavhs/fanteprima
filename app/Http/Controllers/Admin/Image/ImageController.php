<?php

namespace App\Http\Controllers\Admin\Image;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use App\Models\Images;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class ImageController extends AdminController
{
    public function index(){
        if(checkPermission('admin.image.list')){
            $this->setDatatableJSCSS();
            $this->loadImageGalleryPluginJSCSS();
            $breadcrumbData = [
                [
                    'title'=>'Dashboard',
                    'url' => route('dashboard'),
                    'active' => false,
                ],
                [
                    'title' => 'Images',
                    'url'=> '#',
                    'active'=> true,

                ]
            ];

            return view('Admin.Image.index',[
                'page_title' =>'Images',
                'menu' => 'image',
                'breadcrumbData'=> $breadcrumbData,
            ]);

        }else{
            return Redirect::back()->with(['msg_type'=>'error','msg'=>'you are not allowed to access']);
        }
    }
    public function imagesList(Request $request){

        return DataTables::eloquent(Images::query())
            ->editColumn('image',function ($query){
//                return '<strong>'.$query->name.'</strong>';
                return '<img src="'.$query->image_url.'" style="width:100%;"/><strong>'.$query->image.'</strong>';
            })
            ->addColumn('action', function ($query) {
                $links = '';

                if(checkPermission('admin.image.view')) {
                    $links .= '&nbsp;<button type="button" data-image-url="'.$query->image_url.'"  data-toggle="modal" data-target="#imageModal-'.$query->id.'" title="View" class="btn btn-sm btn-primary viewImageBtn"> <i class="fa fa-eye"></i></button>
                                 <div class="modal fade" id="imageModal-'.$query->id.'">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">'.$query->image.'</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                   <img src="'.$query->image_url.'" id="imageUrl" style="width: 100%;" />
                                                </div>
                                                <div class="modal-footer justify-content-between">
                                                    <button type="button" class="btn btn-default closeBtn" data-dismiss="modal">Close</button>

                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>';
                }
                if(checkPermission('admin.image.delete')) {
                    $links .= '&nbsp;<button class="btn btn-sm btn-danger" onclick="deleteImage($(this),'.$query->id.')"><i class="fa fa-trash"></i> </button>';
                }
                return $links;
            })
            ->escapeColumns([])
            ->toJson();
    }

    public function save(Request $request){

        if($request->file('image')){
            $image = new Images();
            $imageName = 'image-'.time().'.png';
            $image->uploadImage($imageName);
            $getImageInfo = getimagesize(asset('public/'.$image->image_dir.'/'.$imageName));
            $data =[
                'user_id'=>Auth::user()->id,
                'image'=>$imageName,
                'width' => isset($getImageInfo[0]) ? $getImageInfo[0] :null,
                'height' => isset($getImageInfo[1]) ? $getImageInfo[1] :null,
            ];
           $status = Images::create($data);
           $response = [
               'status' => 1,
               'message' => 'Image Uploaded Successfully'
           ];
        }else{
            $response = [
                'status' => 0,
                'message' => 'Image not Uploaded'
            ];
        }
        return  response($response);

    }

    public function delete($id){
        $response = [];
        if($image = Images::find($id)){
            $image->unlinkImage();
            $image->delete();
            $response = [
                'status' => 1,
                'message' => 'Image deleted',
            ];
        }else{
            $response = [
                'status' => 0,
                'message' => 'Image not found'
            ];
        }
        return $response;
    }

}
