<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Assets;

class AdminController extends Controller
{
    public function __construct()
    {
        Assets::config([
            'css_dir' => url('public/admin_assets'),
            'js_dir'  => url('public/admin_assets'),
        ]);
        Assets::addJS(
            [
                asset('public/admin_assets/plugins/jquery/jquery.min.js'),
                asset('public/admin_assets/plugins/jquery-ui/jquery-ui.min.js') ,
                asset('public/admin_assets/plugins/bootstrap/js/bootstrap.bundle.min.js'),
                asset('public/admin_assets/plugins/chart.js/Chart.min.js'),
                asset('public/admin_assets/plugins/sparklines/sparkline.js'),
                asset('public/admin_assets/plugins/jqvmap/jquery.vmap.min.js'),
                asset('public/admin_assets/plugins/sweetalert2/sweetalert2.min.js'),
                asset('public/admin_assets/plugins/moment/moment.min.js'),
//                    asset('public/admin_assets/plugins/daterangepicker/daterangepicker.js'),
                asset('public/admin_assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js'),
//                    asset('public/admin_assets/plugins/summernote/summernote-bs4.min.js'),
                asset('public/admin_assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js'),
                asset('public/admin_assets/js/adminlte.js'),
//                    asset('public/admin_assets/js/pages/dashboard.js'),
//                    asset('public/admin_assets/js/demo.js')
                asset('public/admin_assets/plugins/jquery-validation/jquery.validate.min.js'),
                asset('public/admin_assets/plugins/jquery-validation/additional-methods.min.js'),

            ]
        );

        Assets::addCSS(
            [
                asset('public/admin_assets/plugins/fontawesome-free/css/all.min.css'),
                'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
                asset('public/admin_assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css'),
                asset('public/admin_assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css'),
                asset('public/admin_assets/plugins/jqvmap/jqvmap.min.css'),
                asset('public/admin_assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css'),
                asset('public/admin_assets/css/adminlte.min.css'),
                asset('public/admin_assets/css/style.css'),
                asset('public/admin_assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css'),
//                    asset('public/admin_assets/plugins/daterangepicker/daterangepicker.css'),
//                    asset('public/admin_assets/plugins/summernote/summernote-bs4.css')
            ]
        );
    }

    public function setDatatableJSCSS(){
        Assets::addJS(
            [
                asset('public/admin_assets/plugins/datatables/jquery.dataTables.min.js'),
                asset('public/admin_assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js'),
                asset('public/admin_assets/plugins/datatables-responsive/js/dataTables.responsive.min.js'),
                asset('public/admin_assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js'),
            ]
        );
        Assets::addCSS([
            asset('public/admin_assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css'),
            asset('public/admin_assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css'),
        ]);
    }

    public function loadImageGalleryPluginJSCSS(){
        Assets::addCSS(
            [
                asset('public/admin_assets/plugins/ekko-lightbox/ekko-lightbox.css'),
            ]
        );

        Assets::addJS(
            [
                asset('public/admin_assets/plugins/ekko-lightbox/ekko-lightbox.min.js'),
                asset('public/admin_assets/plugins/filterizr/jquery.filterizr.min.js'),
            ]
        );
    }

    public function loadDataTableReorderJS(){

        Assets::addCSS(
            [
                asset('public/admin_assets/plugins/datatables-rowreorder/css/rowReorder.bootstrap4.css'),
            ]
        );

        Assets::addJS(
            [
                asset('public/admin_assets/plugins/datatables-rowreorder/js/dataTables.rowReorder.js'),
                asset('public/admin_assets/plugins/datatables-rowreorder/js/rowReorder.bootstrap4.js'),
            ]
        );
    }
    public function loadSummernote(){
        Assets::addCSS([asset('public/admin_assets/plugins/summernote/summernote-bs4.css')]);
        Assets::addJS([asset('public/admin_assets/plugins/summernote/summernote-bs4.min.js')]);
    }
    public function loadFileInput(){
        Assets::addJS([asset('public/admin_assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js')]);
//            Assets::addCSS([asset('public/admin_assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js')]);
    }

    public function loadChartJS(){
        Assets::addJS([
            asset('public/admin_assets/plugins/flot/jquery.flot.js'),
            asset('public/admin_assets/plugins/flot-old/jquery.flot.resize.min.js'),
            asset('public/admin_assets/plugins/flot-old/jquery.flot.pie.min.js'),
        ]);
    }
    public function loadSelect2(){
        Assets::addCSS([
            asset('public/admin_assets/plugins/select2/css/select2.min.css'),
            asset('public/admin_assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css'),
        ]);
        Assets::addJS([
            asset('public/admin_assets/plugins/select2/js/select2.full.min.js'),
        ]);
    }

    public function loadDashboardJS(){
        Assets::addJS([
            asset('public/admin_assets/plugins/chart.js/Chart.min.js'),
            asset('public/admin_assets/plugins/jqvmap/maps/jquery.vmap.usa.js'),
            asset('public/admin_assets/plugins/jquery-knob/jquery.knob.min.js'),
        ]);
    }
}
