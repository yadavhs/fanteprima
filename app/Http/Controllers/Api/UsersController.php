<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use App\Models\Images;
use App\Models\Role;
use App\Models\Roles;
use App\Models\RoleUser;
use App\Models\RoleUsers;
use App\Models\Users;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class UsersController extends AdminController
{
    public function login(Request $request){
        Log::info('Users : POST Login : ' . json_encode($request->all()));

        $validationRules = [
            'email' => 'required|email|exists:users,email',
            'password' => 'required',
            'device_id' => 'required',
            'device_name' => 'required'
        ];

        $validator = Validator::make($request->all(), $validationRules);

        if ($validator->fails()) {
            $errorMessage = getFormattedError($validator->errors()->messages());
            jsonResponse(0, 'Validation failed', $errorMessage);
        }

        $userExist = User::query()
                ->where('email', $request->email)
                ->where('password', sha1($request->password))
                ->where('is_employee', 1)
                ->first();

        if ($userExist) {

            if($userExist->device_id && $userExist->device_name){
                if($userExist->device_id != $request->device_id){
                    jsonResponse(0, 'Validation Failed', ['error' => 'Device is not registered']);
                }
            }else{

                $deviceExist = User::query()
                    ->where('device_id', $request->device_id)
                    ->first();

                if($deviceExist){
                    jsonResponse(0, 'Validation Failed', ['error' => 'Device already registered from other user']);
                }

                $userExist->device_id = $request->device_id;
                $userExist->device_name = $request->device_name;
                $userExist->update();
            }

            $userObject = getUserResponseObject($userExist);
            jsonResponse(1, 'Welcome to Fanteprima!', $userObject);

        } else {
            jsonResponse(0, 'Validation Failed', ['error' => 'Invalid Credentials']);
        }
    }

    public function getTextures(Request $request){
        Log::info('Users : POST Get Textures : ' . json_encode($request->all()));

        $validationRules = [
            'id' => 'required|exists:users,id',
        ];

        $validator = Validator::make($request->all(), $validationRules);

        if ($validator->fails()) {
            $errorMessage = getFormattedError($validator->errors()->messages());
            jsonResponse(0, 'Validation failed', $errorMessage);
        }

        $images = Images::query()->orderBy('id', 'desc')->paginate(20);

        $data = [];
        foreach ($images as $image) {
            $data[] = getImageResponseObject($image);
        }

        $page = null;
        if (($images->currentPage() < $images->total()) && ($images->lastPage() > $images->currentPage())) {
            $page = $images->currentPage() + 1;
        }

        if($images){
            jsonResponse(1, 'Texture images fetched', $data, ['page' => $page]);
        }else{
            jsonResponse(0, 'No images found');
        }
    }

}
