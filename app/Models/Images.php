<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class Images extends Model
{
    protected $fillable = [
        'user_id',
        'image',
        'width',
        'height',
    ];

    public  function getImageDirAttribute(){
        return 'uploads/images/';
    }
    public  function getImagePathAttribute(){
        return public_path($this->image_dir);
    }

    public  function getImageUrlAttribute(){
        return asset('public/'.$this->image_dir.$this->image);
    }

    public function uploadImage($imageName){
        if(request()->file('image')){
            uploadImage(request()->file('image'),$this->image_path,$imageName);
        }
    }
    public  function unlinkImage(){
        if(File::exists($this->image_path.'/'.$this->image)){
            File::exists($this->image_path.'/'.$this->image);
        }

    }

}
