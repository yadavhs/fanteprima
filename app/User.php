<?php

namespace App;

use App\Models\Roles;
use App\Models\RoleUsers;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\File;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'username',
        'active',
        'slug',
        'bio',
        'is_admin',
        'device_id',
        'device_name',
        'is_employee',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function getImagePathAttribute(){
        return public_path($this->image_dir);
    }
    public function getImageDirAttribute(){
        return 'uploads/user/'.$this->id.'/';
    }
    public function getImageUrlAttribute(){
        return  asset('public/'.$this->image_dir.'/avatar.png');
    }
    public  function uploadImage(){
        if(request()->file('image')) {
            $this->unlinkImage();
            uploadImage(request()->file('image'), $this->image_path, 'avatar.png', '250', '');
        }
    }
    public  function unlinkImage(){
        if(File::exists($this->image_path.'/avatar.png')){

            File::delete($this->image_path.'/avatar.png');
        }

    }
    public function getRoleIdsAttribute(){
        return  RoleUsers::where('user_id',$this->id)->get()->pluck('role_id')->toArray();
    }
    public function getRoleNamesAttribute(){
        return   Roles::whereIn('id',$this->role_ids)->get()->pluck('name')->toArray();
    }
    public function getCheckFileExistsAttribute(){
        return File::exists($this->image_path.'/avatar.png');
    }
}
