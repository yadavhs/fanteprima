<?php
use \Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;


function uploadImage($image, $directory, $filename, $width = null, $height = null){
    if (!is_null($image) && $image->isValid()) {
        if (!File::isDirectory($directory)) {
            File::makeDirectory($directory, 0755, true);
        }

        $image = Image::make($image->getRealpath());
//        $image->orientate();

        if ($width || $height) {
            $image->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $image->save($directory . '/' . $filename);
    }
}

function regenerateThumbnail($image, $directory, $filename, $width = null, $height = null){
    if (!is_null($image) && $image->isValid()) {
        if (!File::isDirectory($directory)) {
            File::makeDirectory($directory, 0755, true);
        }

        $image = Image::make($image->getRealpath());
//        $image->orientate();

        if ($width || $height) {
            $image->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $image->save($directory . '/' . $filename);
    }
}


function uploadImageFromUrl($imageURL, $directory, $filename, $width = null, $height = null)
{
    if (!File::isDirectory($directory)) {
        File::makeDirectory($directory, 0755, true);
    }

    /* Create default size */
    Image::make($imageURL)
        ->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        })
        ->save($directory . '/' . $filename);
}

function checkPermission($permission)
{
return true;
    $currentUser = \Illuminate\Support\Facades\Auth::user();
    if($permission = \App\Models\Permissions::where('name',$permission)->first()){
        $status = \App\Models\RolePermissions::where('permission_id',$permission->id)->whereIn('role_id',$currentUser->role_ids)->get();
        if($status->count() > 0){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}

function getUserResponseObject($user){
    return [
        'id' => $user->id,
        'name' => $user->name,
        'username' => $user->username,
    ];
}

function getImageResponseObject($image){
    return [
        'id' => $image->id,
        'width' => $image->width,
        'height' => $image->height,
        'url' => $image->imageUrl
    ];
}

/**
 * Generate Readable Array
 * From Validator Response
 *
 * @param $rawErrors
 * @return array
 */
function getFormattedError($rawErrors)
{
    //Initialise return array
    $errors = [];

    foreach ($rawErrors as $key => $value){
        $errors[$key] = $rawErrors[$key][0];
    }

    return $errors;
}

/**
 * Print Json Response
 * After Function Completion
 *
 * @param int $status
 * @param int $error_code
 * @param string $message
 * @param array $data
 * @param array $extraData
 */
function jsonResponse($status, $message, $data = null, $extraData = [])
{
    $responseObject = [
        'status' => $status,
//        'error_code' => $error_code,
        'message' => $message
    ];

    if($data){
        $responseObject['data'] = $data;
    }

    if(!empty($extraData)){
        $responseObject = array_merge($responseObject, $extraData);
    }

    //Set output header
    header('Content-Type: application/json');

    //Json Encode
    $json_data = json_encode($responseObject, JSON_UNESCAPED_UNICODE);

    \Illuminate\Support\Facades\Log::info('RESPONSE : ' . str_replace(':null', ':""', $json_data));

    //Convert null values to empty ('') value
    echo str_replace(':null', ':""', $json_data);
    exit();
}

