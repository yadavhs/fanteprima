-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2020 at 07:58 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fanteprima`
--

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `width` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `height` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `user_id`, `image`, `width`, `height`, `created_at`, `updated_at`) VALUES
(1, 1, 'image-1594920603.png', '1254', '836', '2020-07-16 12:00:03', '2020-07-16 12:00:03'),
(2, 1, 'image-1594921912.png', '2000', '1320', '2020-07-16 12:21:54', '2020-07-16 12:21:54');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_07_16_162412_create_images_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('barney8919@hotmail.co.uk', '$2y$10$1373yi467aE1NZKY58Cjh.1lo59KkrNnilpzrMR4SUJn9VnUk.uo.', '2020-06-02 09:40:34'),
('nolan@mailinator.com', '$2y$10$.g8QhTHUKQriP257gFsJIesFypTivSgk5uH10PxXL3AIs07EvmvpW', '2020-06-08 07:35:11'),
('admin@mailinator.com', '$2y$10$hm7c/lJeX5e8NpiFjtAv/.n2aeBPudaJh7DkAkkTkjGRqInN08qya', '2020-06-08 18:47:09'),
('nolannew@mailinator.com', '$2y$10$jI35kTJBrzuL4CEJTZmkjOMHKdgVkVseeHTrQNyW03xLg1POK4L96', '2020-06-08 18:49:37'),
('yadavhs@mailinator.com', '$2y$10$g1AYjizAUOxKh.C1IY3MVu2Hwkn1DncEjSQEmNQBaxDeR3Y21qyjG', '2020-06-09 14:43:23'),
('hiddengems@mailinator.com', '$2y$10$f1UhsBJB4grFFDSo0dX5KOxT/Zz7LamCw8zrclNjJbhlNpCtEvGp.', NULL),
('maria@mailinator.com', '$2y$10$ZO8mICdmfGphIAlTO6bkfuf9A2rY6CHNYEWXdqcfQczmpXsnSyCPK', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin.users.list', 'Admin Users Listings', '2020-06-22 11:31:42', '2020-06-22 06:02:51'),
(2, 'admin.users.create', 'Create Admin Users', '2020-06-22 06:06:28', '2020-06-22 06:06:28'),
(3, 'admin.users.view', 'View Admin Users', '2020-06-22 06:06:55', '2020-06-22 06:06:55'),
(4, 'admin.users.update', 'Update Admin Users', '2020-06-22 06:07:13', '2020-06-22 06:07:13'),
(5, 'admin.users.delete', 'Delete Admin Users', '2020-06-22 06:07:31', '2020-06-22 06:07:31'),
(6, 'admin.roles.list', 'Roles Listing', '2020-06-22 06:07:52', '2020-07-15 11:03:53'),
(7, 'admin.roles.create', 'Create Roles', '2020-06-22 06:08:11', '2020-07-15 11:04:02'),
(8, 'admin.roles.edit', 'Update Roles', '2020-06-22 06:08:37', '2020-07-15 11:04:11'),
(9, 'admin.roles.delete', 'Delete Roles', '2020-06-22 06:08:58', '2020-07-15 11:04:17'),
(10, 'admin.permissions.list', 'Permissions List', '2020-06-22 06:09:29', '2020-06-22 06:09:29'),
(11, 'admin.permissions.create', 'Create Permissions', '2020-06-22 06:09:48', '2020-06-22 06:09:48'),
(12, 'admin.permissions.update', 'Update Permissions', '2020-06-22 06:10:05', '2020-06-22 06:10:05'),
(28, 'admin.employee.users.list', 'List of Employee Users', '2020-07-16 10:26:02', '2020-07-16 10:26:02'),
(29, 'admin.employee.users.create', 'Create Employee User', '2020-07-16 10:26:17', '2020-07-16 10:26:17'),
(30, 'admin.employee.users.update', 'Update Employee Users', '2020-07-16 10:26:35', '2020-07-16 10:26:35'),
(31, 'admin.employee.users.view', 'View Employee Users', '2020-07-16 10:26:56', '2020-07-16 10:26:56'),
(32, 'admin.employee.users.delete', 'Delete Employee Users', '2020-07-16 10:27:14', '2020-07-16 10:27:14'),
(33, 'admin.image.list', 'List of Images', '2020-07-16 11:08:32', '2020-07-16 11:08:32'),
(34, 'admin.image.view', 'View Images', '2020-07-16 11:08:43', '2020-07-16 11:08:43'),
(35, 'admin.image.delete', 'Delete Images', '2020-07-16 11:08:57', '2020-07-16 11:08:57');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(23, 9, 3, '2020-06-18 10:03:39', '2020-06-18 10:03:39', NULL),
(24, 10, 3, '2020-06-18 10:03:39', '2020-06-18 10:03:39', NULL),
(128, 5, 2, '2020-06-18 15:31:31', '2020-06-18 15:31:31', NULL),
(129, 6, 2, '2020-06-18 15:31:31', '2020-06-18 15:31:31', NULL),
(130, 7, 2, '2020-06-18 15:31:31', '2020-06-18 15:31:31', NULL),
(131, 9, 2, '2020-06-18 15:31:31', '2020-06-18 15:31:31', NULL),
(132, 12, 2, '2020-06-18 15:31:31', '2020-06-18 15:31:31', NULL),
(268, 1, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(269, 2, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(270, 3, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(271, 4, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(272, 5, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(273, 6, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(274, 7, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(275, 8, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(276, 9, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(277, 10, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(278, 11, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(279, 12, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(280, 28, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(281, 29, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(282, 30, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(283, 31, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(284, 32, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(285, 33, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(286, 34, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL),
(287, 35, 1, '2020-07-16 11:12:35', '2020-07-16 11:12:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Super Administrator', 'Super Administrator', '2020-06-17 14:28:22', '2020-06-17 14:28:22', NULL),
(2, 'Administrator', 'Administrator', '2020-06-17 14:28:46', '2020-06-17 14:29:35', NULL),
(4, 'User Manager', 'User Manager', '2020-06-17 14:30:47', '2020-06-17 14:30:59', NULL),
(5, 'Employee', 'Employee User', '2020-07-16 10:25:21', '2020-07-16 10:25:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 1, 1, '2020-06-18 07:52:25', NULL, NULL),
(5, 2, 3, '2020-07-15 10:58:34', '2020-07-15 10:58:34', NULL),
(6, 5, 4, '2020-07-16 10:41:40', '2020-07-16 10:41:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1 for default, 2 for facebook, 3 for gmail',
  `bio` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT 0,
  `is_employee` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `remember_token`, `name`, `active`, `created_at`, `updated_at`, `deleted_at`, `username`, `slug`, `login_type`, `bio`, `is_admin`, `is_employee`) VALUES
(1, 'admin@mailinator.com', '$2y$10$RefVba8n0z1kS.xeT83nBeUMy4FCPM3lfzOrw0AqnvpclXjp4Gqeq', NULL, 'Admin', 1, '2020-07-14 17:02:56', NULL, NULL, NULL, 'admin', 1, NULL, 1, 0),
(3, 'demo@mailinator.com', '$2y$10$4NHu4SKh/BFR7ybzfbjQPuNbsj7wjpgF/4a7TGBtRnr5CRUf1PrL2', NULL, 'demo', 1, '2020-07-15 10:58:34', '2020-07-15 16:29:19', NULL, NULL, NULL, 1, NULL, 1, 0),
(4, 'nishit@mailinator.com', '$2y$10$D9TdZAFm1CjOs3UcfmE3lOj71AE1zIjqDcXfrFzgOeYSS3CjiulmC', NULL, 'Nishit Yadav', 1, '2020-07-16 10:41:40', '2020-07-16 10:47:45', NULL, 'nishit', NULL, 1, 'Employee', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_roles` (`role_id`),
  ADD KEY `role_user_users` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`) USING BTREE,
  ADD UNIQUE KEY `users_slug_unique` (`slug`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=288;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
