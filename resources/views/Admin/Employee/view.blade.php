@extends('Admin.layout.layout')
@section('content')
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card card-header-custom-color">
                    <div class="card-header">
                        <h3 class="card-title">Employee Detail</h3>

                        <div class="card-tools">
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-sm-3">
                                <div class="info-box bg-light">
                                    <div class="info-box-content">
                                        <img src="{{ $employee->image_url }}" style="width: 100%"/>
                                        <span class="info-box-text text-center text-muted">Name</span>
                                        <span class="info-box-number text-center text-muted mb-0">{{ $employee->name }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-9">
                                <div class="row">
                                    <div class="col-12 col-sm-6">
                                        <div class="info-box bg-light">
                                            <div class="info-box-content">
                                                <span class="info-box-text text-center text-muted">Email</span>
                                                <span class="info-box-number text-center text-muted mb-0">{{ $employee->email }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <div class="info-box bg-light">
                                            <div class="info-box-content">
                                                <span class="info-box-text text-center text-muted">Username</span>
                                                <span class="info-box-number text-center text-muted mb-0">{{ $employee->username }}<span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-6">
                                        <div class="info-box bg-light">
                                            <div class="info-box-content">
                                                <span class="info-box-text text-center text-muted">Country</span>
                                                <span class="info-box-number text-center text-muted mb-0">{{ $employee->country ? $employee->country:'N/A' }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <div class="info-box bg-light">
                                            <div class="info-box-content">
                                                <span class="info-box-text text-center text-muted">Role</span>
                                                <span class="info-box-number text-center text-muted mb-0">{{ implode(',',$employee->role_names) }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="info-box bg-light">
                                    <div class="info-box-content">
                                        <span class="info-box-text text-center text-muted">Bio</span>
                                        <span class="info-box-number text-center text-muted mb-0">{{ $employee->bio }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
@endsection
@push('scripts')
<script>

</script>
@endpush
