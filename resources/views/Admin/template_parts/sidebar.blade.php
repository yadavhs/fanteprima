<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-danger elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="{{ asset('public/admin_assets/img/AdminLTELogo.png') }}"
             alt="AdminLTE Logo"
             class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Fanteprima</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('public/admin_assets/img/avatar5.png') }}" class="img-circle elevation-2"
                     alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Admin</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item ">
                    <a href="{{ url('admin/dashboard') }}" class="nav-link {{ $menu == 'dashboard' ? 'active':'' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                            {{--                            <span class="right badge badge-danger">New</span>--}}
                        </p>
                    </a>
                </li>
                @if(checkPermission('admin.users.list'))
                    <li class="nav-item">
                        <a href="{{ url('admin/admin-users') }}"
                           class="nav-link {{ isset($submenu) && $submenu =='admin_users' ?'active':'' }}">
                            <i class="far fa-user nav-icon"></i>
                            <p>Admin Users</p>
                        </a>
                    </li>
                @endif

                @if(checkPermission('admin.employee.users.list'))
                    <li class="nav-item ">
                        <a href="{{ url('admin/employee') }}" class="nav-link {{ $menu == 'employee' ? 'active':'' }}">
                            <i class="nav-icon fas fa-user"></i>
                            <p>
                                Employees
                                {{--                            <span class="right badge badge-danger">New</span>--}}
                            </p>
                        </a>
                    </li>
                @endif
                @if(checkPermission('admin.image.list'))
                    <li class="nav-item ">
                        <a href="{{ url('admin/image') }}" class="nav-link {{ $menu == 'image' ? 'active':'' }}">
                            <i class="nav-icon fas fa-images"></i>
                            <p>
                                Images
                                {{--                            <span class="right badge badge-danger">New</span>--}}
                            </p>
                        </a>
                    </li>
                @endif

                {{--            @if(checkPermission('admin.users.list')  || checkPermission('admin.roles.list') || checkPermission('admin.permissions.list'))--}}
                {{--                    <li class="nav-item has-treeview {{ $menu == 'authentication' ? 'menu-open':'' }}">--}}
                {{--                        <a href="#" class="nav-link {{ $menu == 'authentication' ? 'active':'' }}">--}}
                {{--                            <i class="nav-icon fas fa-key"></i>--}}
                {{--                            <p>--}}
                {{--                                Authentication--}}
                {{--                                <i class="right fas fa-angle-left"></i>--}}
                {{--                            </p>--}}
                {{--                        </a>--}}
                {{--                        <ul class="nav nav-treeview ">--}}
                {{--                            @if(checkPermission('admin.users.list'))--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ url('admin/admin-users') }}"--}}
                {{--                                       class="nav-link {{ isset($submenu) && $submenu =='admin_users' ?'active':'' }}">--}}
                {{--                                        <i class="far fa-user nav-icon"></i>--}}
                {{--                                        <p>Admin Users</p>--}}
                {{--                                    </a>--}}
                {{--                                </li>--}}
                {{--                            @endif--}}
                {{--                            @if(checkPermission('admin.roles.list'))--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('roles') }}"--}}
                {{--                                       class="nav-link {{ isset($submenu) && $submenu =='roles' ?'active':'' }}">--}}
                {{--                                        <i class="far fa-user nav-icon"></i>--}}
                {{--                                        <p>Roles</p>--}}
                {{--                                    </a>--}}
                {{--                                </li>--}}
                {{--                            @endif--}}
                {{--                            @if(checkPermission('admin.permissions.list'))--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('permissions')}}"--}}
                {{--                                       class="nav-link {{ isset($submenu) && $submenu =='permissions' ?'active':'' }}">--}}
                {{--                                        <i class="far fa-user nav-icon"></i>--}}
                {{--                                        <p>Permissions</p>--}}
                {{--                                    </a>--}}
                {{--                                </li>--}}
                {{--                            @endif--}}
                {{--                        </ul>--}}
                {{--                    </li>--}}
                {{--                @endif--}}
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
