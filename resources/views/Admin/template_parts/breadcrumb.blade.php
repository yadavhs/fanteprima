

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ isset($page_title) ? $page_title:null }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        @foreach($breadcrumbData as $bData)
                            @if(!$bData['active'])
                                <li class="breadcrumb-item"><a href="{{ $bData['url'] }}">{{ $bData['title'] }}</a></li>
                            @else
                                <li class="breadcrumb-item active">{{ $bData['title'] }}</li>
                            @endif

                        @endforeach
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
