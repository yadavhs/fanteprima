@extends('Admin.layout.layout')

@section('content')
    <div class="row">
        <div class="col-12">

            <!-- Default box -->
            <div class="card card-header-custom-color">
                <div class="card-header">
                    <h3 class="card-title">Roles List</h3>

                    <div class="card-tools">
                        @if(checkPermission('admin.roles.create'))
                            <a href="{{ route('roles.create') }}" class="btn btn-sm btn-black"><i class="fa fa-plus"></i> &nbsp;Add</a>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="rolesList">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Created</th>
                                    @if(checkPermission('admin.roles.edit') || checkPermission('admin.roles.delete'))
                                        <th>Action</th>
                                    @endif
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var role_table ;
        $(document).ready(function(){
            role_table =   $('#rolesList').DataTable({
                // "processing": true,
                "serverSide": true,
                "ajax": "{{ route('rolesDatatableList') }}",
                "columns" :[
                    { "data": "id" },
                    { "data": "name" },
                    { "data": "description" },
                    { "data": "created" },
                    @if(checkPermission('admin.roles.edit') || checkPermission('admin.roles.delete'))
                         { "data": "action" }
                    @endif

                ],
            });
        });
        function deleteRole(ele,id) {
            // var confirm = ;
            if (confirm('Are you sure you want to delete this item?')) {
                $.ajax({
                    url: "{{ url('admin/role/delete') }}/"+id,
                    method: 'get',
                    // data: {
                    //     priority:position,
                    //     category_id:categoryID
                    // },
                    success: function (data) {
                        // franchise_table.ajax.reload();
                        if (data.status) {
                            alertMSG('success','Item Deleted Successfully');
                            role_table.ajax.reload();
                        }else{
                            alertMSG('error','Item not Deleted');
                        }
                    },
                    error: function (xhr) {
                        alertMSG('error','Internal Server Error');
                    }
                });
            }
        }
    </script>
@endpush
