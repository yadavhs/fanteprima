@extends('Admin.layout.layout')

@section('content')
    <div class="row">
        <div class="col-12">
            <form class="" method="post" id="roleForm" action="{{ isset($role) ?route('roles.save',$role->id) :route('roles.save') }}">
               {{ csrf_field() }}
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ isset($role) ? 'Edit':'Create' }} Role</h3>

                        <div class="card-tools">
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Permissions</label>
                            <select  class="form-control select2bs4" multiple name="permissions[]">
                                @foreach($permissions as $id=> $name)
                                    <option value="{{ $id }}" {{ isset($role->permission_ids) && in_array($id,$role->permission_ids) ? 'selected':'' }} >{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                           <div class="form-group">
                               <label for="">Name</label>
                               <input required type="text" name="name" class="form-control" value="{{ isset($role) ? $role->name:'' }}" id="name" placeholder="Enter Name">
                           </div>
                           <div class="form-group">
                               <label for="">Description</label>
                               <textarea name="description" required class="form-control">{{ isset($role) ? $role->description:'' }}</textarea>
                           </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary-custom">Submit</button>
                    </div>
                </div>
                <!-- /.card -->
            </form>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.select2bs4').select2({
                theme: 'bootstrap4',
                placeholder:'Add Permissions',
            })
            //     .on('change',function(e){
            //     $(this).valid();
            // });
            $('#roleForm').validate({
                rules: {
                    // 'permissions[]': {
                    //     minlength: 1,
                    // },
                },
                messages: {
                    name: {
                        required: "Please Enter  Name",
                    },
                    // 'permissions[]':{
                    //     required: 'Add Permissions',
                    // },
                    description:{
                        required: 'Pl   ease Enter description',
                    }
                },
                errorElement: 'span',
            });
        });
    </script>
@endpush
