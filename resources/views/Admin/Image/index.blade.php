@extends('Admin.layout.layout')
@section('content')
    <div class="row">
        <div class="col-12">
            <!-- Default box -->
            <div class="card card-header-custom-color">
                <div class="card-header">
                    <h3 class="card-title">Images List</h3>
                    <div class="card-tools">
{{--                        @if(checkPermission('admin.employee.users.create'))--}}
{{--                            <a href="{{ route('employee.create') }}" class="btn btn-sm btn-black"><i class="fa fa-plus"></i>&nbsp; Add</a>--}}
{{--                        @endif--}}
                    </div>
                </div>
                <div class="card-body">
                    <form enctype="multipart/form-data" >
                        <div class="form-group">
                            <label>Choose  Image</label>
                            <br>
                            <div class="file btn btn-sm btn-primary" style="background-color: #dc3545 !important;">
                                Choose File
                                <input type="file" id="profileImage" name="image"/>
                            </div>

                            <div class="image-div" id="imagePreview" style="display: none;">
                                <br>
                                <img src="" alt="Avatar" class="custom-image" style="width:100%">
                                <div class="middle">
                                    <button class="custom-img-text btn btn-xs btn-danger" type="button" onclick="removeIMG($(this))"><i class="fa fa-trash"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" id="imagesList">
                            <thead>
                            <tr class="text-center">
                                <th>ID</th>
                                <th style="width: 20%;">Name</th>
                                <th>Width</th>
                                <th>Height</th>
                                @if(checkPermission('admin.image.view') || checkPermission('admin.image.delete'))
                                    <th>Action</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody class="text-center">

                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">

                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->
        </div>
    </div>


    <!-- /.modal -->

@endsection
@push('scripts')
    <script>
        // $('.viewImageBtn').on('click',function () {
        //     var image_url = $(this).data('image-url');
        //     console.log(image_url);
        //     $('#imageUrl').attr('src',image_url);
        //     $('#imageUrl').show();
        // });
        // $('.closeBtn').on('click',function () {
        //     $('#imageUrl').attr('src','');
        //     $('#imageUrl').hide();
        // });
        $('#profileImage').on('change',function(){
            var file = this.files[0]
            var formData = new FormData();
            formData.append('image',file);
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            $.ajax({
                type:'POST',
                url:'{{ route('images.save') }}',
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                data:formData,
                success:function (data) {
                    if(data.status){
                        location.reload();
                    }else{
                        alert(data.message);
                    }
                    // console.log(data);
                },
                error:function (xhr) {
                    alert(xhr);
                },

            });
            // var imageUrl =  readURL(this,$('#imagePreview').find('img'));
            //
            // $('#imagePreview').show();
        });
        var user_table;
        $(document).ready(function(e){
            user_table =   $('#imagesList').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('imagesDatatableList') }}",
                "columns" :[
                    { "data": "id" },
                    { "data": "image" },
                    { "data": "width" },
                    { "data": "height" },
                    @if(checkPermission('admin.image.view')  || checkPermission('admin.image.delete'))
                    { "data": "action" }
                    @endif

                ],
            });
        });

        function deleteImage(ele,id) {
            // var confirm = ;
            if (confirm('Are you sure you want to delete this item?')) {
                $.ajax({
                    url: "{{ url('admin/image/delete') }}/"+id,
                    method: 'get',
                    // data: {
                    //     priority:position,
                    //     category_id:categoryID
                    // },
                    success: function (data) {
                        // franchise_table.ajax.reload();
                        if (data.status) {
                            alertMSG('success','Image Deleted Successfully');
                            user_table.ajax.reload();
                        }else{
                            alertMSG('error','Image not Deleted');
                        }
                    },
                    error: function (xhr) {
                        alertMSG('error','Internal Server Error');
                    }
                });
            }
        }
        function removeIMG(ele) {
            ele.parents('.image-div').find('.custom-image').attr('src','');
            ele.parents('.image-div').hide();
        }
        function readURL(input,previewImageEle) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(previewImageEle).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }
    </script>
@endpush
