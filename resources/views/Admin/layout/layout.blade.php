<!DOCTYPE html>
<html>
<head>
    @include('Admin.template_parts.head')
</head>
<body class="hold-transition sidebar-mini layout-fixed ">
<div id="pageLoader" style="display: none;">
    <div class=" text-center" style="position: absolute;top: 50%;left: 50%;">
        <div class="spinner-grow text-secondary" style="color: #dc3545 !important;" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-secondary" style="color:#dc3545 !important;" role="status">
            <span class="sr-only"></span>
        </div>
        <div class="spinner-grow text-secondary" style="color: #dc3545 !important;" role="status">
            <span class="sr-only"></span>
        </div>
        <div class="spinner-grow text-secondary" style="color: #dc3545 !important;" role="status">
            <span class="sr-only"></span>
        </div>
    </div>
</div>
<div class="wrapper">

    <!-- Navbar -->
            @include('Admin.template_parts.header')
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
   @include('Admin.template_parts.sidebar')

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
                    @include('Admin.template_parts.breadcrumb')
        <!-- /.content-header -->
            <section class="content">
                <div class="container-fluid">
                    <!-- Small boxes (Stat box) -->
                @yield('content')
                </div><!-- /.container-fluid -->
            </section>
    </div>
    <!-- /.content-wrapper -->
        @include('Admin.template_parts.footer')

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<?= Assets::js() ?>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->


<script>
    $.widget.bridge('uibutton', $.ui.button);
    $.validator.setDefaults({
        // submitHandler: function () {
        //     return true;
        //     // alert( "Form successful submitted!" );
        // },
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    $(document).ajaxStart(function(){
        $('[data-toggle="tooltip"]').tooltip();
        // Show loader container
        $('#pageLoader').show();
        // $('#pageLoader span').text('Please Wait...');
    });

    $(document).ajaxComplete(function(){
        // Hide loader container
        $('#pageLoader').hide();
        // $('#pageLoader span').text('');
    });
</script>

<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });
    function alertMSG(type,msg){
        // const Toast = Swal.mixin({
        //     toast: true,
        //     position: 'top',
        //     showConfirmButton: false,
        //     timer: 3000
        // });
        if(type == 'danger'){
            type == 'error';
        }
        Toast.fire({
            icon: type,
            title: msg
        });
    }
    @if(Session::has('msg') || Session::has('msg_type'))
    Toast.fire({
        icon: '{{ Session::get('msg_type') }}',
        title: '{{ Session::get('msg') }}'
    })
    @endif
</script>
@stack('scripts')
</body>
</html>

